package edu.uchicago.cs.quiz.pkuprys;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class QuizActivity extends Activity {

    public static final String NUMBER_CORRECT = "edu.uchicago.cs.quiz.pkuprys.NUMBER_CORRECT";
    public static final String NUMBER_INCORRECT = "edu.uchicago.cs.quiz.pkuprys.NUMBER_INCORRECT";
    public static final String TOTAL_ANSWERED = "edu.uchicago.cs.quiz.pkuprys.TOTAL_ANSWERED";
    public static final String NAME = "edu.uchicago.cs.quiz.pkuprys.NAME";

    private Button mExitButton, mStartButton;
    private String mName;
    EditText mNameEditText;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);

        mNameEditText = (EditText) findViewById(R.id.editName);

        //exit button
        mExitButton = (Button) findViewById(R.id.exitButton);
        mExitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        //start button
        mStartButton = (Button) findViewById(R.id.startButton);
        mStartButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startMe();
            }
        });
    }

    private void startMe() {
        mName = mNameEditText.getText().toString();
        QuizTracker.getInstance().setName(mName);
        askQuestion(1);
    }

    private void askQuestion(int number) {
        QuizTracker.getInstance().setQuestionNum(number);
        Intent intent = new Intent(QuizActivity.this, QuestionActivity.class);
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_start, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menuExit:
                finish();
                return true;
            case R.id.menuStart:
                startMe();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
