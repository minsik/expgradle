package edu.uchicago.cs.quiz.pkuprys.test;


import android.app.Activity;
import android.app.Instrumentation;
import android.test.ActivityInstrumentationTestCase2;
import android.test.TouchUtils;
import android.view.KeyEvent;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import java.util.Random;

import edu.uchicago.cs.quiz.pkuprys.Question;
import edu.uchicago.cs.quiz.pkuprys.QuestionActivity;
import edu.uchicago.cs.quiz.pkuprys.QuizActivity;
import edu.uchicago.cs.quiz.pkuprys.QuizTracker;
import edu.uchicago.cs.quiz.pkuprys.R;
import edu.uchicago.cs.quiz.pkuprys.ResultActivity;


public class QuestionActivityTest extends
        ActivityInstrumentationTestCase2<QuestionActivity> {


    private QuizTracker mQuizTracker;
    private TextView mQuestionNumberTextView;
    private RadioGroup mQuestionRadioGroup;
    private Button mSubmitButton;
    private Button mQuitButton;
    private Activity mActivity = null;

    public QuestionActivityTest() {
        super(QuestionActivity.class);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        //this calls onCreate of the activity
        mActivity = getActivity();
        mQuizTracker = QuizTracker.getInstance();

        mQuestionRadioGroup = (RadioGroup) mActivity.findViewById(R.id.radioAnswers);
        mSubmitButton = (Button) mActivity.findViewById(R.id.submitButton);
        mQuitButton = (Button) mActivity.findViewById(R.id.quitButton);

    }

    public void testQuit() {

        Instrumentation.ActivityMonitor monitor =
                getInstrumentation().addMonitor(ResultActivity.class.getName(), null, false);

        TouchUtils.tapView(this, mQuitButton);

        Activity nextActivity = getInstrumentation().waitForMonitorWithTimeout(monitor, 5);

        assertEquals(nextActivity.getComponentName().getShortClassName(), ".ResultActivity");
        Watch.watch();
        nextActivity.finish();

    }

    public void testSubmitAnswer(){


       // Question question = mActivity.getTheQuestion();
        int nNumQuestion = mQuizTracker.getQuestionNum();

        //A RadioGroup is an MVC mechanism
        //This MVC mechanism occurs on a background thread, so you must
        // enqueue this action to the eventQueue of the UI thread like so.
        //ViewPager, Spinner, and any other animated Views will also require a separate thread
        new Thread(new Runnable() {
            @Override
            public void run() {
                mActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Random random = new Random();
                        RadioButton radioButton = (RadioButton) mQuestionRadioGroup.getChildAt(random.nextInt(5));
                        radioButton.setChecked(true);

                    }
                });
            }
        }).start();

        Watch.watch();
        TouchUtils.clickView(this, mSubmitButton);
        //give it some time to update the UI before reading back the data in mQuizTracker
        Watch.delay(1500);
        assertEquals( nNumQuestion + 1, mQuizTracker.getQuestionNum());

    }









}
